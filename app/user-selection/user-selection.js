myApp.controller('userSelectionController', ['$scope', '$location', '$rootScope', function ($scope, $location) {

    var self = this;
    self.redirectToIndividual = function () {
        $location.path('/index');

    };
    self.redirectToCompany = function () {
        $location.path('/second');

    }

}]);
