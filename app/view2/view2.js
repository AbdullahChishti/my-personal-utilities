"use strict";

myApp.controller("CompanyController", ["$scope", "$http", "$location", "$rootScope", function ($scope, $http, $location) {

    var num;
    var self = this;
    self.company = {};
    self.step = 0;
    self.company.entityType = "COMPANY";
    $scope.ButtonText = "JOIN NOW";
    $scope.resendText='Resend OTP';

    self.directTo__SignIn = function () {
        $location.path("/business-signin");
    };

    self.step1 = function () {
        $scope.ButtonText = "Please wait...";
        $http({
            method: "POST",
            url: "http://34.214.153.95:9090/webservices/signUp/verify-email",
            headers: {"Content-Type": "application/json"},
            params: {
                "email": self.company.myEmail
            }
        }).then(function mySuccess(response) {
            $scope.$emit("eventName", {message: 2});
            self.step = 2;

        }, function myError(response) {
            alert("There is an error : " + response.data.errors);
            $scope.ButtonText = 'Try again';

        });

    };

    self.step2 = function () {

        self.step = 3;
    };


    // self.step1 = function () {
    //     var observable = Rx.Observable.fromPromise(
    //         $http({
    //             method: 'POST',
    //             url: 'http://34.214.153.95:7070/webservices/signUp/company/email-name-password',
    //             params: {
    //                 'email': self.company.myEmail,
    //                 'company-name': self.company.companyName,
    //                 '
    // password': self.company.myPassword
    //             }

    //         })
    //     );
    //
    //     observable.subscribe(
    //         function (data) {
    //             alert("success !!");
    //             $scope.data = data;
    //         },
    //         function (err) {
    //             alert("Failure");
    //             $scope.error = err.message;
    //         }
    //     );
    // };

    // self.step1 = function () {
    //     {
    //         alert(self.company);
    //         console.log(self.company);
    //         $http({
    //             method: 'POST',
    //             url: 'http://34.214.153.95:7070/webservices/signUp/company/email-name-password',
    //             headers: {'Content-Type': 'application/json'},
    //             params: {
    //                 'email': self.company.myEmail,
    //                 'company-name': self.company.companyName,
    //                 'password': self.company.myPassword
    //             }
    //         }).then(function mySuccess(response) {
    //             alert("success");
    //             self.company.uuid = response.data.data;
    //             alert(self.company.uuid);
    //             self.step = 2;
    //
    //         }, function myError(response) {
    //             alert("There is an error : " + response.data.message);
    //         });
    //
    //     }
    // };

    self.step3 = function () {
        // alert("now you are at step 2");
        self.step = 4;
        // $http({
        //     method: 'POST',
        //     url: 'http://34.214.153.95:7070/webservices/signUp/company/email-name-password',
        //     headers: {'Content-Type': 'application/json'},
        //     params: {
        //         'email': self.company.myEmail
        //     }
        // }).then(function mySuccess(response) {
        //     alert("success");
        //     self.step = 2;
        //
        // }, function myError(response) {
        //     alert("There is an error : " + response.data.message);
        // });
    };
    self.step4 = function () {
        if (self.company.myPassword === self.company.confirmPassword) {
            $http({
                method: "POST",
                url: "http://34.214.153.95:9090/webservices/signUp/company/",
                headers: {"Content-Type": "application/json"},
                params: {
                    "email": self.company.myEmail,
                    "first-name": self.company.firstName,
                    "last-name": self.company.lastName,
                    "company-name": self.company.companyName,
                    "password": self.company.myPassword
                }
            }).then(function mySuccess(response) {
                self.company.uuid = response.data.data;
                num = response.data.data;
                self.step = 5;

            }, function myError(response) {
                alert("There is an error : " + response.data.message);
            });
            self.step = 4;
        }
        else {
            alert("passwords do not match. Please try again!");
        }
        // $http({
        //     method: 'POST',
        //     url: 'http://34.214.153.95:7070/webservices/signUp/company/email-name-password',
        //     headers: {'Content-Type': 'application/json'},
        //     params: {
        //         'email': self.company.myEmail
        //     }
        // }).then(function mySuccess(response) {
        //     alert("success");
        //     self.step = 2;
        //
        // }, function myError(response) {
        //     alert("There is an error : " + response.data.message);
        // });
    };

    self.step5 = function () {

        $http({
            method: "POST",
            url: "http://34.214.153.95:9090/webservices/utility/mobile-no",
            headers:
                {
                    "Content-Type": 'application/json'
                },
            params: {
                "mobile-no": self.company.mobileNo,
                "entity-type": self.company.entityType,
                "uuid": num
            }
        }).then(function mySuccess(response) {
            self.step = 6;

        }, function myError(response) {
            alert("something went wrong");
        });
    };

    self.step6 = function () {
        $http({
            method: 'GET',
            url: 'http://34.214.153.95:9090/webservices/utility/verify-otp',
            headers:
                {
                    'Content-Type': 'application/json'
                },
            params: {
                'uuid': num,
                'otp': self.company.OTP,
                'entity-type': self.company.entityType

            }
        }).then(function mySuccess(response) {
            alert("verification successful! Welcome");
            self.step = 7;

        }, function myError(response) {
            alert('something went wrong')

        });


    };
    self.resendOTP = function () {

        $scope.resendText ='sending new OTP';

        $http({
            method: 'POST',
            url: 'http://34.214.153.95:9090/webservices/utility/mobile-no',
            headers:
                {
                    'Content-Type': 'application/json'
                },
            params: {
                'mobile-no': self.company.mobileNo,
                'entity-type': self.company.entityType,
                'uuid': num
            }
        }).then(function mySuccess(response) {
            // self.otpMsg = 1;
            $scope.resendText ='A new OTP has been sent. Please check your device';


        }, function myError(response) {
            // self.otpMsg = 2;
            $scope.resendText ='Something went wrong! Try again';

        });
    };


}]);
//     self.step3 = function () {
//         self.OTP = self.company.otp1 + "" + self.company.otp2 + "" + self.company.otp3 + "" + self.company.otp4 + ""
//         alert(self.OTP);
//         $http({
//             method: 'POST',
//             url: 'http://34.214.153.95:7070/webservices/signUp/verify-otp',
//             headers: {'Content-Type': 'application/json'},
//             params: {
//                 'otp': self.OTP,
//                 'entity-type': self.company.entityType,
//                 'uuid': self.company.uuid
//             }
//         }).then(function mySuccess(response) {
//             console.log(response.data);
//             alert(self.company.uuid);
//             alert("success !");
//             self.step = 3;
//
//         }, function myError(response) {
//             alert("There is an error : " + response.data.message);
//         });
//
//     };
//
//     self.ResendOtp = function () {
//         alert("Resending the otp now ");
//         $http({
//             method: 'GET',
//             url: 'http://34.214.153.95:7070/webservices/signUp/resend-otp',
//             headers: {'Content-Type': 'application/json'},
//             params: {
//                 'mobile-no': '92' + self.company.mobileNumber,
//                 'entity-type': self.company.entityType,
//                 'uuid': self.company.uuid
//             }
//         }).then(function mySuccess(response) {
//             alert("OTP has been successfully resent.");
//
//         }, function myError(response) {
//             alert("There is an error : " + response.data.message);
//
//         });
//
//     };
//
// }]);
//
