"use strict";

myApp.controller('BusinessSigninController', ['$scope', '$http', '$location', function ($scope, $http, $location) {

    var self = this;
    self.redirectTo__SignUp = function () {
        $location.path('/business-onboarding');
    };

}]);

