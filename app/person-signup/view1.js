myApp.controller('indexController', ['$scope', '$http', function ($scope, $http) {


    var self = this;
    self.individual = {};
    self.step = 1;
    self.individual.entityType = 'INDIVIDUAL';

    self.step1 = function () {
        {
            alert(self.individual);
            console.log(self.individual);
            $http({
                method: 'POST',
                url: 'http://34.214.153.95:7070/webservices/signUp/individual/email-name-password',
                headers: {'Content-Type': 'application/json'},
                params: {
                    'email': self.individual.myEmail,
                    'first-name': self.individual.firstName,
                    'last-name': self.individual.lastName,
                    'password': self.individual.myPassword
                }
            }).then(function mySuccess(response) {
                alert("success");
                self.individual.uuid = response.data.data;
                alert(self.individual.uuid);
                self.step = 2;

            }, function myError(response) {
                alert("no");
                alert(response.errors);
            });

        }
    };

    self.step2 = function () {
        alert("now you are at step 2");
        alert("and this is uuid " + self.individual.uuid);
        $http({
            method: 'POST',
            url: 'http://34.214.153.95:7070/webservices/signUp/mobile-no',
            headers: {'Content-Type': 'application/json'},
            params: {
                'mobile-no': self.individual.mobileNumber,
                'country-code': self.individual.countryCode,
                'entity-type': self.individual.entityType,
                'uuid': self.individual.uuid
            }
        }).then(function mySuccess(response) {
            alert("success");
            self.step = 3;

        }, function myError(response) {
            alert(response.data.errors);
        });

    };

    self.step3 = function () {
        self.OTP = self.individual.otp1 + "" + self.individual.otp2 + "" + self.individual.otp3 + "" + self.individual.otp4 + ""
        alert(self.OTP);
        $http({
            method: 'POST',
            url: 'http://34.214.153.95:7070/webservices/signUp/verify-otp',
            headers: {'Content-Type': 'application/json'},
            params: {
                'otp': self.OTP,
                'entity-type': self.individual.entityType,
                'uuid': self.individual.uuid
            }
        }).then(function mySuccess(response) {
            console.log(response.data);
            alert(self.individual.uuid);
            alert("success !");
            self.step = 3;

        }, function myError(response) {
            alert("this is an error " + response.data.error);
        });

    };

    self.ResendOtp = function () {
        alert("Resending the otp now ");
        $http({
            method: 'GET',
            url: 'http://34.214.153.95:7070/webservices/signUp/resend-otp',
            headers: {'Content-Type': 'application/json'},
            params: {
                'mobile-no': '92' + self.individual.mobileNumber,
                'entity-type': self.individual.entityType,
                'uuid': self.individual.uuid
            }
        }).then(function mySuccess(response) {
            alert("OTP has been successfully resent.");

        }, function myError(response) {
            alert(response.data.errors);
            alert("There was some error. Please try again later.");
        });

    };

}]);