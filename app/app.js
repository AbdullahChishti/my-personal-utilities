var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.when('/individual-onboarding', {
        templateUrl: 'view1/view1.html',
        controller: 'indexController as $ctrl'
    }).when('/business-onboarding', {
        templateUrl: 'view2/view2.html',
        controller: 'CompanyController as $ctrl'
    }).when('/user-dashboard', {
        templateUrl: 'view3/view3.html',
        controller: 'userDashboardController as $ctrl'
    }).when('/user-selection', {
        templateUrl: 'user-selection/user-selection.html',
        controller: 'userSelectionController as $ctrl'
    }).when('/business-signin', {
        templateUrl: 'view2/business-signin.html',
        controller: 'BusinessSigninController as $ctrl'
    }).otherwise('/business-onboarding');

});

