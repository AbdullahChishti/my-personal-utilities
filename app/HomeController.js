'use strict';

myApp.controller('HomeController', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {

    var self = this;
    $rootScope.count = self.enabler;
    self.enabler = 1;

    $scope.$on('eventName', function (event, args) {
        self.enabler = args.message;

    });

    self.directTo__Signup = function () {
        $location.path('/business-onboarding');
    };

    self.directTo__SignIn = function () {
        $location.path('/business-signin');
    };
}]);